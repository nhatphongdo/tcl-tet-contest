﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Term.aspx.cs" Inherits="TCL_Tet_Contest.Term" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="Content/jquery.jscrollpane.css" rel="stylesheet" />
    <script src="Scripts/mwheelIntent.js"></script>
    <script src="Scripts/jquery.mousewheel.js"></script>
    <script src="Scripts/jquery.jscrollpane.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.menu .term').addClass('selected');

            $('#term-panel').jScrollPane();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="common-page term">
        <div class="left-column">
            <h2>thể lệ tham dự</h2>
            <div id="term-panel">
                <h3>I- Đối tượng dự thi:</h3>
                <ul>
                    <li>Công dân Việt Nam không giới hạn giới tính, độ tuổi và vùng miền.</li>
                    <li>Thời gian: từ ngày 15/01/2013 đến hết ngày 09/02/2013</li>
                </ul>
                <h3>II- Cách thức tham dự:</h3>
                <ul>
                    <li>Người tham dự truy cập vào Facebook Fanpage TCL Vietnam: www.facebook.com/tclvietnam và truy cập vào ứng dụng “Tết về nhà - Mua Tivi làm quà”.</li>
                    <li>Người tham dự đăng ảnh gia đình trực tiếp ngay trên ứng dụng và điền đầy đủ thông tin cá nhân để hoàn tất.</li>
                    <li>Người tham dự kêu gọi bạn bè bình chọn (Like) cho bức ảnh của mình.</li>
                    <li>Những bức ảnh đúng tiêu chí và nhận được sự bình chọn cao nhất sẽ nhận được giải thưởng.</li>
                    <li>Từ ngày 15/01/2013 đến 03/02/2013 sẽ có các giải thưởng ngày và giải thưởng tuần. Đến hết ngày 09/02/2013 sẽ công bố giải thưởng Đặc biệt (Xem chi tiết phần “Giải thưởng”).</li>
                </ul>
                <h3>III- Tiêu chí ảnh dự thi:</h3>
                <ul>
                    <li>Ảnh chụp các thành viên trong gia đình thể hiện được sự sum họp, hạnh phúc của cả gia đình.</li>
                    <li>Ảnh có sự xuất hiện của TV TCL (nhìn thấy rõ logo TCL) và thể hiện được không khí ngày Tết sẽ có cơ hội nhận các giải thưởng có giá trị lớn.</li>
                    <li>Ảnh nguyên gốc hoặc chỉ được chỉnh sửa về màu sắc, không sử dụng các công cụ chỉnh sửa ảnh để làm thay đổi bố cục và nội dung bức ảnh.</li>
                    <li>Không giới hạn thời điểm bức ảnh được chụp.</li>
                    <li>Không giới hạn thiết bị chụp ảnh.</li>
                </ul>
                <h3>IV- Giải thưởng:</h3>
                <ul>
                    <li>A- Cơ cấu giải thưởng:
                        <ol>
                            <li>Giải thưởng “Ngày hạnh phúc” (20 giải)
                                <br />
                                - Dành cho chủ nhân của bức ảnh thể hiện cảnh sum họp gia đình ấm áp và nhận được kết quả bình chọn cao nhất mỗi ngày (Từ ngày 15/01/2013 đến hết ngày 03/02/2013): Điện thoại Alcatel 927D.
                            </li>
                            <li>Giải thưởng ảnh đẹp chụp cùng Tivi TCL (3 giải)
                                <br />
                                - Dành cho chủ nhân của bức ảnh chụp cùng Tivi TCL (thấy rõ logo TCL) nhận được kết quả bình chọn cao nhất mỗi tuần (Trong 3 tuần từ 15/01/2013 đến 03/02/2013): Tivi TCL LED 29T2100
                            </li>
                            <li>Giải thưởng Đặc biệt “Tết về nhà - Mua Tivi làm quà” (1 giải)
                                <br />
                                - Dành cho chủ nhân của bức ảnh nhận được kết quả bình chọn cao nhất đến hết ngày 09/02/2013 (29 Tết): Tivi TCL LED 32E5300D
                            </li>
                        </ol>
                        * Lưu ý: 
                        <ul>
                            <li>Mỗi người chơi chỉ được nhận giải thưởng “Ngày hạnh phúc” 1 lần duy nhất, người thắng giải của ngày trước không được tham gia giải ngày sau.</li>
                            <li>Người thắng giải ngày có thể tham gia giải tuần nhưng chỉ được nhận giải thưởng tuần 1 lần duy nhất, người thắng giải của tuần trước không được tham tiếp vào tuần sau.</li>
                            <li>Tuy nhiên người thắng giải ngày, tuần vẫn có thể tham gia tranh giải thưởng Đặc biệt.</li>
                        </ul>
                    </li>
                    <li>B- Thời gian và cách thức:
                        <ul>
                            <li>Thời gian công bố kết quả ngày (Lượt bình chọn của ngày tính đến trước 24h ngày hôm đó). Ngày hôm sau sẽ công bố kết quả bình chọn của ngày hôm trước và trao giải.</li>
                            <li>Thời gian công bố kết quả tuần (Lượt bình chọn của tuần tính đến trước 24h ngày Chủ nhật tuần đó): các ngày 22-29/01/2013 công bố giải thưởng tuần 1 và 2, ngày 05/02/2013 công bố giải thưởng tuần 3.</li>
		 <li>Thời gian công bố giải Đặc biệt: 10/02/2013</li>
                            <li>Thời gian nhận giải: đến hết ngày 31/02/2013 (Nhận giải trong giờ hành chính)</li>
                            <li>Cách thức nhận giải: ngay sau khi công bố giải thưởng, BTC sẽ trực tiếp liên hệ người chơi để hướng dẫn cách thức nhận giải.</li>
                        </ul>
                    </li>
                </ul>
                <h3>V- Quy định chung:</h3>
                <ul>
                    <li>Các ảnh tham gia dự thi phải là tác phẩm chưa được đăng tải/công bố trên bất kỳ phương tiện thông tin đại chúng nào, chưa từng tham gia cuộc thi nào, và không thuộc bất kì nguồn sở hữu nào.</li>
                    <li>BTC sẽ không chịu trách nhiệm về việc tranh chấp tác quyền và tính pháp lý của tác phẩm. Người dự thi chịu mọi trách nhiệm liên quan đến vấn đề tác quyền của ảnh dự thi. </li>
                    <li>BTC được sử dụng ảnh cho các mục đích quảng bá và truyền thông cho cuộc thi. Ảnh sử dụng trong các mục đích quảng cáo tiếp thị sẽ được BTC liên lạc và thương thảo bản quyền sử dụng ảnh cùng tác giả. </li>
                    <li>Ảnh vi phạm thể lệ cuộc thi hay thuần phong mỹ tục Việt Nam sẽ bị loại bỏ mà không cần thông báo trước. </li>
                    <li>Nếu có bằng chứng hiển nhiên người chơi gian lận và sao chép tác phẩm thì kết quả của người thắng cuộc sẽ bị tước bỏ.</li>
                    <li>Nếu phát hiện tác phẩm trúng giải vi phạm thể lệ cuộc thi hoặc đến 24 giờ trước khi trao giải mà BTC vẫn chưa liên lạc được với người đoạt giải, BTC có quyền rút lại giải để trao cho người chiến thắng kế tiếp. </li>
                    <li>BTC có quyền hủy kết quả giải thưởng trong trường hợp có tranh chấp và phần lỗi thuộc về người đoạt giải. </li>
                    <li>Nếu có một vấn đề phát sinh trước trong hoặc sau cuộc thi, mà vấn đề này nằm ngoài quy định đang có, thì BTC sẽ giữ toàn quyền thảo luận để đưa ra quyết định xử lý vấn đề phát sinh đó.</li>
                    <li>Quyết định của BTC là quyết định cuối cùng. Mọi tranh chấp, khiếu nại, thắc mắc về quyết định của BTC đều không có giá trị.</li>
                </ul>
            </div>
        </div>
        <div class="right-column">
            <h2>giải thưởng</h2>
            <img src="Images/term-prize.png" alt="" />
        </div>
    </div>
</asp:Content>
