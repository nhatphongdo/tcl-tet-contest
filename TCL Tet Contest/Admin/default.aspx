﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Site1.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="TCL_Tet_Contest.Admin._default" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:MultiView runat="server" ID="AdminViews">
        <asp:View runat="server" ID="LoginView">
            <asp:Login ID="Login1" runat="server" Font-Names="Arial" Style="margin: auto;" OnAuthenticate="Login1_Authenticate">
            </asp:Login>
        </asp:View>
        <asp:View runat="server" ID="MainView">
            <asp:Panel ID="RadPane1" runat="server" BorderStyle="None" Font-Names="Arial" GroupingText="Thống kê">
                <div style="width: 100%; float: left; display: block;">
                    Có
                        <strong>
                            <asp:Literal runat="server" ID="TotalUsers">0</asp:Literal></strong>
                    người tham gia với 
                       <strong>
                           <asp:Literal runat="server" ID="TotalPhotos">0</asp:Literal></strong>
                    ảnh
                       <br />
                    <label class="float-left" style="padding-bottom:3px;">Filter by "Upload date"</label>
                    <label class="float-left" style="padding-bottom:3px;">&nbsp;&nbsp;from&nbsp;&nbsp;</label>
                    <div class="float-left" style="padding-bottom:3px;">
                        <dx:ASPxDateEdit ID="FromDate" runat="server"></dx:ASPxDateEdit>
                    </div>
                    <label class="float-left" style="padding-bottom:3px;">&nbsp;&nbsp;to&nbsp;&nbsp;</label>
                    <div class="float-left" style="padding-bottom:3px;">
                        <dx:ASPxDateEdit ID="ToDate" runat="server"></dx:ASPxDateEdit>
                    </div>
                    <div class="float-left" style="margin-left: 10px; ">
                        <dx:ASPxButton ID="FilterButton" runat="server" Text="Filter" OnClick="FilterButton_Click"></dx:ASPxButton>
                    </div>
                    <div class="float-left" style="margin-left: 10px;">
                        <dx:ASPxButton ID="ClearFilter" runat="server" Text="Clear Filter" OnClick="ClearFilter_Click"></dx:ASPxButton>
                    </div>
                </div>
            </asp:Panel>
            <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" Width="100%" DataSourceID="SqlDataSource1" KeyFieldName="ID" Theme="Default" OnCustomButtonCallback="ASPxGridView1_CustomButtonCallback">
                <Columns>
                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Visible="False">
                        <ClearFilterButton Visible="True">
                        </ClearFilterButton>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn FieldName="ID" ReadOnly="True" Visible="False" VisibleIndex="1">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="FullName" VisibleIndex="2" Width="40px">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataImageColumn FieldName="PhotoPath" VisibleIndex="3">
                        <PropertiesImage ImageUrlFormatString="~/{0}" ImageWidth="120px">
                        </PropertiesImage>
                    </dx:GridViewDataImageColumn>
                    <dx:GridViewDataCheckColumn FieldName="IsTCL" VisibleIndex="7">
                    </dx:GridViewDataCheckColumn>
                    <dx:GridViewDataDateColumn FieldName="UploadedDate" VisibleIndex="4">
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataTextColumn FieldName="Likes" VisibleIndex="5">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="IP" VisibleIndex="6">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataCheckColumn FieldName="IsPublished" VisibleIndex="8">
                    </dx:GridViewDataCheckColumn>
                    <dx:GridViewCommandColumn ButtonType="Button" Caption="Published" ShowSelectCheckbox="False" VisibleIndex="9">
                        <CustomButtons>
                            <dx:GridViewCommandColumnCustomButton ID="Publish" Text="Publish/UnPublish">
                                <Image ToolTip="Publish/UnPulish" Url="~/Images/Publish.png"></Image>
                            </dx:GridViewCommandColumnCustomButton>
                        </CustomButtons>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewCommandColumn ButtonType="Button" Caption="IsTCL" ShowSelectCheckbox="False" VisibleIndex="10">
                        <CustomButtons>
                            <dx:GridViewCommandColumnCustomButton ID="IsTCL" Text="TCL/NonTCL">
                                <Image ToolTip="TCL/NonTCL" Url="~/Images/Publish.png"></Image>
                            </dx:GridViewCommandColumnCustomButton>
                        </CustomButtons>
                    </dx:GridViewCommandColumn>
                </Columns>
                <SettingsPager PageSize="20">
                    <PageSizeItemSettings Visible="True">
                    </PageSizeItemSettings>
                </SettingsPager>
                <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                <Styles>
                    <Cell HorizontalAlign="Center" VerticalAlign="Middle">
                    </Cell>
                </Styles>
            </dx:ASPxGridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TCL_Tet_ContestConnectionString %>" SelectCommand="SELECT * FROM [Photo] LEFT JOIN [User] ON [Photo].UserID = [User].ID WHERE (([UploadedDate] &gt;= @UploadedDate) AND ([UploadedDate] &lt;= @UploadedDate2))">
                <SelectParameters>
                    <asp:ControlParameter ControlID="FromDate" DefaultValue="10/10/2012" Name="UploadedDate" PropertyName="Value" Type="DateTime" />
                    <asp:ControlParameter ControlID="ToDate" DefaultValue="10/10/2020" Name="UploadedDate2" PropertyName="Value" Type="DateTime" />
                </SelectParameters>
            </asp:SqlDataSource>
        </asp:View>
    </asp:MultiView>
</asp:Content>
