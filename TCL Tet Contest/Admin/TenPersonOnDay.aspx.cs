﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

namespace TCL_Tet_Contest.Admin
{
    public partial class TenPersonOnDay : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AdminViews.SetActiveView(LoginView);
            var cookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (cookie != null && !string.IsNullOrEmpty(cookie.Value))
            {
                var formTicket = FormsAuthentication.Decrypt(cookie.Value);
                if (!formTicket.Expired && formTicket.Name.ToLower() == "admin")
                {
                    AdminViews.SetActiveView(MainView);
                    LoadData();
                }
            }
        }

        public void LoadData()
        {
            var endToday = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59, 999);
            var entities = new TCL_Tet_ContestEntities();
            var data = (from p in entities.Photos
                        //where p.UploadedDate.Day == now.Day && p.UploadedDate.Month == now.Month && p.UploadedDate.Year == now.Year
                        select new
                        {
                            p.ID,
                            p.IP,
                            p.IsPublished,
                            p.IsTCL,
                            p.PhotoPath,
                            p.User.FullName,
                            p.UploadedDate,
                            p.Likes,
                            NumberLike = p.Likes1.Count(l => l.CreatedDate <= endToday),
                            p.User.Address,
                            p.User.Email,
                            p.User.PhoneNumber,
                            p.User.FacebookName
                        })
                       .OrderByDescending(p => p.NumberLike);
            ASPxGridView1.DataSource = data.ToList();
            ASPxGridView1.DataBind();
        }

        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {
            if (Login1.UserName.ToLower() == "admin" && Login1.Password == "tcltetcontest123#@!")
            {
                FormsAuthentication.SetAuthCookie("Admin", true);
                Response.Redirect("Default.aspx");
            }
        }


        protected void ASPxGridView1_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewCustomButtonCallbackEventArgs e)
        {

            if (e.ButtonID == "Publish")
            {
                int rowid = Convert.ToInt32(ASPxGridView1.GetRowValues(e.VisibleIndex, "ID"));
                if (rowid > 0)
                {
                    using (var entities = new TCL_Tet_ContestEntities())
                    {
                        var data = entities.Photos.Where(p => p.ID == rowid).SingleOrDefault();
                        data.IsPublished = !data.IsPublished;
                        entities.SaveChanges();
                        LoadData();
                    }
                }

            }
            if (e.ButtonID == "IsTCL")
            {
                int rowid = Convert.ToInt32(ASPxGridView1.GetRowValues(e.VisibleIndex, "ID"));
                if (rowid > 0)
                {
                    using (var entities = new TCL_Tet_ContestEntities())
                    {
                        var data = entities.Photos.Where(p => p.ID == rowid).SingleOrDefault();
                        data.IsTCL = !data.IsTCL;
                        entities.SaveChanges();
                        LoadData();
                    }
                }

            }
        }

        protected void btDayAt_Click(object sender, EventArgs e)
        {
            if (DateAt.Value != null)
            {
                DateTime date;
                if (DateTime.TryParse(DateAt.Value.ToString(), out date))
                {
                    var endToday = new DateTime(date.Year, date.Month, date.Day, 23, 59, 59, 999);
                    var entities = new TCL_Tet_ContestEntities();
                    var data = (from p in entities.Photos
                                select new
                                {
                                    p.ID,
                                    p.IP,
                                    p.IsPublished,
                                    p.IsTCL,
                                    p.PhotoPath,
                                    p.User.FullName,
                                    p.UploadedDate,
                                    p.Likes,
                                    NumberLike = p.Likes1.Count(l => l.CreatedDate <= endToday),
                                    p.User.Address,
                                    p.User.Email,
                                    p.User.PhoneNumber,
                                    p.User.FacebookName
                                })
                               .OrderByDescending(p => p.NumberLike);

                    ASPxGridView1.DataSource = data.ToList();
                    ASPxGridView1.DataBind();
                }
            }
        }
    }
}