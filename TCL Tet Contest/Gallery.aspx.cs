﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TCL_Tet_Contest
{
    public partial class Gallery : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (var entities = new TCL_Tet_ContestEntities())
                {
                    if (!string.IsNullOrEmpty(Request["order"]))
                    {
                        OrderType.SelectedValue = Request["order"];
                    }

                    if (!string.IsNullOrEmpty(Request["id"]))
                    {
                        var id = 0;
                        if (int.TryParse(Request["id"], out id))
                        {
                            var photo = entities.Photos.FirstOrDefault(p => p.ID == id);
                            if (photo != null)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "showImageFunc",
                                                                    string.Format(
                                                                        "<script type='text/javascript'>showImage({0}, '{1}', {2}, '{3}');</script>",
                                                                        photo.ID, photo.PhotoPath, photo.Likes, photo.User.FullName),
                                                                    false);
                            }
                        }
                    }
                }
            }

            using (var entities = new TCL_Tet_ContestEntities())
            {
                var photos = entities.Photos.Where(p => p.IsPublished);

                // Filter by text
                if (!string.IsNullOrEmpty(SearchValue.Text))
                {
                    photos = photos.Where(p => p.User.FullName.ToLower().Contains(SearchValue.Text.ToLower()));
                }

                // Order
                if (OrderType.SelectedValue == "vote")
                {
                    photos = photos.OrderByDescending(p => p.Likes);
                }
                else if (OrderType.SelectedValue == "time")
                {
                    photos = photos.OrderByDescending(p => p.UploadedDate);
                }
                else
                {
                    photos = photos.Where(p => p.IsTCL == true).OrderByDescending(p => p.Likes);
                }

                // Paging properties
                var pageSize = 8;
                var totalItems = photos.Count();
                var pageCount = totalItems / pageSize;
                if (totalItems % pageSize != 0)
                {
                    ++pageCount;
                }

                // Paging
                var page = 1;
                if (!int.TryParse(Request["page"], out page))
                {
                    page = 1;
                }
                photos = photos.Skip((page - 1) * pageSize).Take(pageSize);

                var result = photos.Select(p => new
                                                    {
                                                        p.ID,
                                                        p.PhotoPath,
                                                        p.Likes,
                                                        Fullname = p.User.FullName
                                                    }).ToList();

                ItemsRepeater.DataSource = result;
                ItemsRepeater.DataBind();

                var pages = new int[pageCount];
                for (var i = 1; i <= pageCount; i++)
                {
                    pages[i - 1] = i;
                }
                PagingNumber.DataSource = pages;
                PagingNumber.DataBind();
            }
        }
    }
}