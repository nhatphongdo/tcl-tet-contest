﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Gallery.aspx.cs" Inherits="TCL_Tet_Contest.Gallery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="Scripts/jquery.cj_object_scaler.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.menu .gallery').addClass('selected');

            $('.items img').each(function () {
                $(this).cjObjectScaler({
                    method: 'fit',
                    fade: 550
                });
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="common-page gallery">
        <div class="order-box">
            <label for="OrderType">Tìm theo</label>
            <div class="round-box">
                <asp:DropDownList runat="server" ID="OrderType" ClientIDMode="Static" AutoPostBack="True">
                    <Items>
                        <asp:ListItem Text="gần nhất" Value="time"></asp:ListItem>
                        <asp:ListItem Text="bình chọn" Value="vote"></asp:ListItem>
                        <asp:ListItem Text="TV TCL" Value="tcl"></asp:ListItem>
                    </Items>
                </asp:DropDownList>
            </div>
        </div>
        <div class="headline">
            Hãy chia sẻ những khoảnh khắc sum họp
            <br />
            đầm ấm bên gia đình bạn cùng với Tv TCL
        </div>
        <div class="search-box">
            <div class="round-box">
                <asp:TextBox runat="server" ID="SearchValue" AutoPostBack="True" Placeholder="Tìm kiếm"></asp:TextBox>
            </div>
        </div>
        <div class="clear"></div>
        <ul class="items">
            <asp:Repeater runat="server" ID="ItemsRepeater">
                <ItemTemplate>
                    <li class="<%# Container.ItemIndex % 4 == 3 ? "last":"" %>">
                        <a href="javascript:void(0);" onclick="showImage(<%# Eval("ID") %>, '<%# Eval("PhotoPath") %>', <%# Eval("Likes") %>, '<%# Eval("Fullname") %>')">
                            <img alt="" src="<%# Eval("PhotoPath") %>" />
                        </a>
                        <div style="padding-top: 5px;">
                            Gia đình bạn
                            <span class="like"><%# Eval("Likes") %> Like</span>
                        </div>
                        <div class="name">
                            <%# Eval("Fullname") %>
                        </div>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
        <div class="clear"></div>
        <ul class="paging">
            <asp:Repeater runat="server" ID="PagingNumber">
                <ItemTemplate>
                    <li class="<%# (Request["page"] == Container.DataItem.ToString() || (string.IsNullOrEmpty(Request["page"]) && (int)Container.DataItem == 1)) ? "selected" : "" %>">
                        <a href="Gallery.aspx?page=<%# Container.DataItem %>&order=<%= OrderType.SelectedValue %>">
                            <%# Container.DataItem %>
                        </a>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>

        <div class="detail-background">
            <div class="detail-item">
                <a class="close" href="javascript:void(0);" onclick="closePopup();"></a>
                <div class="image-container">
                    <img alt="" />
                </div>
                <div style="float: left; color: #333; font-family: Arial; font-size: 12px; padding: 0px 20px; width: 140px;">
                    Bức ảnh của gia đình bạn
                    <br />
                    <span id="fullname"></span>
                    <br />
                    <span class="like-text"><span id="like-text"></span>&nbsp;Like</span>
                </div>
                <a class="like"></a>
                <a class="share"></a>
                <div id="like-message" style="float: right; width: auto; font-size: 12px; padding-right: 20px;"></div>
            </div>
        </div>
    </div>
</asp:Content>
