﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TCL_Tet_Contest
{
    public partial class Upload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                ErrorMessage.Text = string.Empty;

                if (!FileUpload.HasFile)
                {
                    ErrorMessage.Text = "<label class='error'>Bạn phải chọn tập tin ảnh để tải lên</label>";
                }
                else
                {
                    if (!FileUpload.FileName.ToLower().EndsWith(".jpg") &&
                        !FileUpload.FileName.ToLower().EndsWith(".jpeg") &&
                        !FileUpload.FileName.ToLower().EndsWith(".png"))
                    {
                        ErrorMessage.Text = "<label class='error'>Tập tin ảnh phải có định dạng JPEG hoặc PNG</label>";
                    }
                    else if (FileUpload.PostedFile.ContentLength > 1024768)
                    {
                        ErrorMessage.Text = "<label class='error'>Tập tin ảnh phải có dung lượng không quá 1 MB</label>";
                    }
                    else
                    {
                        var facebookID = Session["FacebookID"];
                        var fileName = string.Format("Upload/{0}_{1}.jpg", facebookID, DateTime.Now.Ticks);
                        try
                        {
                            var bitmap = Bitmap.FromStream(FileUpload.PostedFile.InputStream);

                            var propertyItems = bitmap.PropertyItems;
                            foreach (var property in propertyItems)
                            {
                                if (property.Id == 274)
                                {
                                    // Orientation
                                    switch (property.Value[0])
                                    {
                                        case 1:
                                            // Keep as original
                                            break;
                                        case 2:
                                            // Flip X
                                            bitmap.RotateFlip(RotateFlipType.RotateNoneFlipX);
                                            break;
                                        case 3:
                                            // Rotate right 90 degree
                                            bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
                                            break;
                                        case 4:
                                            // Flip Y
                                            bitmap.RotateFlip(RotateFlipType.RotateNoneFlipY);
                                            break;
                                        case 5:
                                            // Rotate right 90 degree and flip X
                                            bitmap.RotateFlip(RotateFlipType.Rotate90FlipX);
                                            break;
                                        case 6:
                                            // Rotate right 90 degree
                                            bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
                                            break;
                                        case 7:
                                            // Rotate left 90 degree and flip X
                                            bitmap.RotateFlip(RotateFlipType.Rotate270FlipX);
                                            break;
                                        case 8:
                                            // Rotate left 90 degree
                                            bitmap.RotateFlip(RotateFlipType.Rotate270FlipNone);
                                            break;
                                    }
                                }
                            }

                            var width = bitmap.Width;
                            var height = bitmap.Height;

                            if (width > 1000)
                            {
                                height = height * 1000 / width;
                                width = 1000;
                            }

                            if (height > 1000)
                            {
                                width = width * 1000 / height;
                                height = 1000;
                            }

                            var resizedBitmap = new Bitmap(bitmap, width, height);
                            resizedBitmap.Save(Server.MapPath(fileName), System.Drawing.Imaging.ImageFormat.Jpeg);

                            FilePath.Value = fileName;
                            Thumbnail.ImageUrl = fileName;
                        }
                        catch (Exception exc)
                        {
                            ErrorMessage.Text =
                                "<label class='error'>Xảy ra lỗi trong quá trình tải ảnh lên. Bạn hãy tải lại trang và thử lại.</label>";
                            return;
                        }
                    }
                }
            }
            else
            {
                UploadViews.SetActiveView(FileView);
            }
        }

        protected void UploadPhoto_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(FilePath.Value))
            {
                ErrorMessage.Text = "<label class='error'>Bạn phải chọn tập tin ảnh để tải lên</label>";
                return;
            }

            if (YesSelection.Checked == false && NoSelection.Checked == false)
            {
                ErrorMessage.Text = "<label class='error'>Bạn phải chọn loại Tivi</label>";
                return;
            }

            using (var entities = new TCL_Tet_ContestEntities())
            {
                var facebookID = (Session["FacebookID"] ?? string.Empty).ToString();
                var user = entities.Users.FirstOrDefault(u => u.FacebookID == facebookID);
                if (user == null)
                {
                    // Do not have user
                    Response.Redirect("Default.aspx");
                    return;
                }

                var photo = entities.Photos.FirstOrDefault(p => p.UserID == user.ID);

                photo = new Photo();
                photo.IP = Utility.GetIPAddress();
                photo.IsPublished = true;
                photo.IsTCL = YesSelection.Checked;
                photo.PhotoPath = FilePath.Value;
                photo.UploadedDate = DateTime.Now;
                photo.UserID = user.ID;
                entities.Photos.Add(photo);
                entities.SaveChanges();

                // Move to step 2
                UploadViews.SetActiveView(InformationView);
            }
        }

        protected void SubmitInformation_Click(object sender, EventArgs e)
        {
            using (var entities = new TCL_Tet_ContestEntities())
            {
                var facebookID = (Session["FacebookID"] ?? string.Empty).ToString();
                var user = entities.Users.FirstOrDefault(u => u.FacebookID == facebookID);
                if (user == null)
                {
                    // Do not have user
                    Response.Redirect("Default.aspx");
                    return;
                }

                // Validate
                if (string.IsNullOrEmpty(Fullname.Text))
                {
                    ErrorMessage2.Text = "<label class='error'>Bạn chưa nhập Họ và Tên</label>";
                    return;
                }

                if (string.IsNullOrEmpty(Email.Text))
                {
                    ErrorMessage2.Text = "<label class='error'>Bạn chưa nhập Địa chỉ Email</label>";
                    return;
                }

                var regexEmail =
                    new System.Text.RegularExpressions.Regex(
                        @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
                if (!regexEmail.IsMatch(Email.Text))
                {
                    ErrorMessage2.Text = "<label class='error'>Địa chỉ Email không hợp lệ</label>";
                    return;
                }

                var oldUser = entities.Users.FirstOrDefault(u => u.Email.ToLower() == Email.Text.ToLower());
                if (oldUser != null && oldUser.ID != user.ID)
                {
                    ErrorMessage2.Text = "<label class='error'>Địa chỉ Email này đã được sử dụng</label>";
                    return;
                }

                if (string.IsNullOrEmpty(Address.Text))
                {
                    ErrorMessage2.Text = "<label class='error'>Bạn chưa nhập Địa chỉ</label>";
                    return;
                }

                if (string.IsNullOrEmpty(PhoneNumber.Text))
                {
                    ErrorMessage2.Text = "<label class='error'>Bạn chưa nhập Số điện thoại</label>";
                    return;
                }

                var regexPhoneNumber = new System.Text.RegularExpressions.Regex(@"^[0-9]+$");
                if (!regexPhoneNumber.IsMatch(PhoneNumber.Text) || PhoneNumber.Text.Length < 6 ||
                    PhoneNumber.Text.Length > 15)
                {
                    ErrorMessage2.Text = "<label class='error'>Số điện thoại không hợp lệ</label>";
                    return;
                }

                if (string.IsNullOrEmpty(IDNumber.Text))
                {
                    ErrorMessage2.Text = "<label class='error'>Bạn chưa nhập Số CMND</label>";
                    return;
                }

                var regexIDNumber = new System.Text.RegularExpressions.Regex(@"^[0-9]+$");
                if (!regexIDNumber.IsMatch(IDNumber.Text) || IDNumber.Text.Length < 9 || IDNumber.Text.Length > 9)
                {
                    ErrorMessage2.Text = "<label class='error'>Số CMND không hợp lệ</label>";
                    return;
                }

                oldUser = entities.Users.FirstOrDefault(u => u.IDNumber == IDNumber.Text);
                if (oldUser != null && oldUser.ID != user.ID)
                {
                    ErrorMessage2.Text = "<label class='error'>Số CMND này đã được sử dụng</label>";
                    return;
                }

                // Update information
                user.FullName = Fullname.Text;
                user.Email = Email.Text;
                user.Address = Address.Text;
                user.PhoneNumber = PhoneNumber.Text;
                user.IDNumber = IDNumber.Text;
                entities.SaveChanges();

                Response.Redirect("Gallery.aspx");
            }
        }
    }
}