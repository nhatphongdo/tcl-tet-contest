﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Result.aspx.cs" Inherits="TCL_Tet_Contest.Result" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="Content/jquery.jscrollpane.css" rel="stylesheet" />
    <script src="Scripts/mwheelIntent.js"></script>
    <script src="Scripts/jquery.mousewheel.js"></script>
    <script src="Scripts/jquery.jscrollpane.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.menu .result').addClass('selected');

            $('.prize-menu .daily').click();

            $('#list-daily').jScrollPane();
        });

        function show(id) {
            $('#daily').hide();
            $('#weekly').hide();
            $('#big').hide();
            $('#' + id).fadeIn();

            $('.prize-menu a').removeClass("selected");
            $('.prize-menu a.' + id).addClass("selected");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="common-page prize">
        <div class="prize-menu">
            <a class="daily" href="javascript:void(0);" onclick="show('daily')">Giải thưởng ngày</a>
            <a class="weekly" href="javascript:void(0);" onclick="show('weekly')">Giải thưởng tuần</a>
            <a class="big" href="javascript:void(0);" onclick="show('big')">Giải thưởng lớn</a>
        </div>
        <div class="clear"></div>
        <div id="daily">
            <div id="list-daily">
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <td>Ngày</td>
                            <td>Họ & tên</td>
                            <td>Tên facebook</td>
                            <td>Số lượt bình chọn</td>
                        </tr>
                    </thead>
                    <tr>
                        <td>15-01</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>16-01</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>17-01</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>18-01</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>19-01</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>20-01</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>21-01</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>22-01</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>23-01</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>24-01</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>25-01</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>26-01</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>27-01</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>28-01</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>29-01</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>30-01</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>31-01</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>01-02</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>02-02</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>03-02</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>04-02</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>05-02</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>06-02</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>07-02</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>08-02</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>09-02</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="weekly">
            <table cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <td>Tuần</td>
                        <td>Họ & tên</td>
                        <td>Tên facebook</td>
                        <td>Số lượt bình chọn</td>
                    </tr>
                </thead>
                <tr>
                    <td>1</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
        <div id="big">
        </div>
    </div>
</asp:Content>
