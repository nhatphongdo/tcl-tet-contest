﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json.Linq;

namespace TCL_Tet_Contest
{
    public partial class SiteMaster : MasterPage
    {
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;

        protected void Page_Init(object sender, EventArgs e)
        {
            // The code below helps to protect against XSRF attacks
            var requestCookie = Request.Cookies[AntiXsrfTokenKey];
            Guid requestCookieGuidValue;
            if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
            {
                // Use the Anti-XSRF token from the cookie
                _antiXsrfTokenValue = requestCookie.Value;
                Page.ViewStateUserKey = _antiXsrfTokenValue;
            }
            else
            {
                // Generate a new Anti-XSRF token and save to the cookie
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
                Page.ViewStateUserKey = _antiXsrfTokenValue;

                var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                {
                    HttpOnly = true,
                    Value = _antiXsrfTokenValue
                };
                if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                {
                    responseCookie.Secure = true;
                }
                Response.Cookies.Set(responseCookie);
            }

            Page.PreLoad += master_Page_PreLoad;
        }

        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
            }
            else
            {
                // Validate the Anti-XSRF token
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                    || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["FacebookID"] = "100000297139222";
            //Session["LikedPage"] = true;
            if (Session["FacebookID"] == null || Session["LikedPage"] == null || (bool)Session["LikedPage"] == false || !string.IsNullOrEmpty(Request["signed_request"]))
            {
                // Not log in or like yet
                try
                {
                    var payload = Request["signed_request"].Split('.')[1];
                    var encoding = new UTF8Encoding();
                    var decodedJson = payload.Replace("=", string.Empty).Replace('-', '+').Replace('_', '/');
                    var base64JsonArray =
                        Convert.FromBase64String(decodedJson.PadRight(
                            decodedJson.Length + (4 - decodedJson.Length % 4) % 4, '='));
                    var json = encoding.GetString(base64JsonArray);

                    var o = JObject.Parse(json);

                    if (o.SelectToken("oauth_token") == null || o.SelectToken("user_id") == null)
                    {
                        // Not login
                        if (!Request.Url.PathAndQuery.ToLower().Contains("/default.aspx"))
                        {
                            Response.Redirect("Default.aspx");
                        }

                        return;
                    }

                    var userID = o.SelectToken("user_id").Value<string>();
                    Session.Add("FacebookID", userID);

                    var pageID = o.SelectToken("page.id") == null ? string.Empty : o.SelectToken("page.id").Value<string>();
                    var liked = o.SelectToken("page.liked") != null && o.SelectToken("page.liked").Value<bool>();

                    Session.Remove("LikedPage");
                    if (pageID == "100442153442138" && liked == true)
                    {
                        Session.Add("LikedPage", true);
                    }

                    if (o.SelectToken("app_data") != null && !string.IsNullOrEmpty(o.SelectToken("app_data").Value<string>()))
                    {
                        Response.Redirect(string.Format("Gallery.aspx?id={0}", o.SelectToken("app_data").Value<string>()));
                    }
                    else if (liked == true && Request.Url.PathAndQuery.ToLower().Contains("/default.aspx"))
                    {
                        // Save user if needed
                        using (var entities = new TCL_Tet_ContestEntities())
                        {
                            var user = entities.Users.FirstOrDefault(u => u.FacebookID == userID);
                            if (user == null)
                            {
                                user = new User
                                {
                                    FacebookID = userID,
                                    IP = Utility.GetIPAddress(),
                                    CreatedOn = DateTime.Now
                                };

                                entities.Users.Add(user);
                                entities.SaveChanges();
                            }
                        }

                        Response.Redirect("Home.aspx");
                    }
                }
                catch (Exception exc)
                {
                    if (!Request.Url.PathAndQuery.ToLower().Contains("/default.aspx"))
                    {
                        Response.Redirect("Default.aspx");
                    }
                }
            }
            else
            {
                if (Request.Url.PathAndQuery.ToLower().Contains("/default.aspx"))
                {
                    Response.Redirect("Home.aspx");
                }
            }
        }
    }
}