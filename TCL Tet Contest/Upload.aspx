﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Upload.aspx.cs" Inherits="TCL_Tet_Contest.Upload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="Scripts/jquery.cj_object_scaler.js"></script>
    <script src="Scripts/jquery.validate.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.menu .post').addClass('selected');

            initFileUploads();

            $('.thumb img').cjObjectScaler({
                method: 'fit',
                fade: 550
            });

            // Validate inputs
            $('form').validate({
                errorLabelContainer: $("form div.error"),
                onkeyup: false,
                onfocusout: false,
                invalidHandler: function (form, validator) {
                    $('form div.error label:not(.error)').remove();
                }
            });

            $('#Fullname').rules("add", {
                required: true,
                messages: {
                    required: "Bạn chưa nhập Họ và Tên"
                }
            });
            $('#Email').rules("add", {
                required: true,
                email: true,
                messages: {
                    required: "Bạn chưa nhập Địa chỉ email",
                    email: "Địa chỉ email không hợp lệ"
                }
            });
            $('#Address').rules("add", {
                required: true,
                messages: {
                    required: "Bạn chưa nhập Địa chỉ"
                }
            });
            $('#PhoneNumber').rules("add", {
                required: true,
                number: true,
                minlength: 6,
                maxlength: 15,
                messages: {
                    required: "Bạn chưa nhập Số điện thoại",
                    number: "Số điện thoại không hợp lệ",
                    minlength: "Số điện thoại không hợp lệ",
                    maxlength: "Số điện thoại không hợp lệ"
                }
            });
            $('#IDNumber').rules("add", {
                required: true,
                number: true,
                minlength: 9,
                maxlength: 9,
                messages: {
                    required: "Bạn chưa nhập Số CMND",
                    number: "Số CMND không hợp lệ",
                    minlength: "Số CMND không hợp lệ",
                    maxlength: "Số CMND không hợp lệ"
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="common-page upload">
        <asp:MultiView runat="server" ID="UploadViews" ActiveViewIndex="-1">
            <asp:View runat="server" ID="FileView">
                <div class="headline"></div>
                <div class="thumb">
                    <div>
                        <asp:Image runat="server" ID="Thumbnail" AlternateText="" ImageUrl="Images/thumbholder.png" />
                    </div>
                </div>
                <div class="upload-button">
                    <asp:HiddenField runat="server" ID="FilePath" />
                    <asp:FileUpload runat="server" ID="FileUpload" />
                </div>
                <div class="error">
                    <asp:Literal runat="server" ID="ErrorMessage"></asp:Literal>
                </div>
                <div class="content">
                    Bức ảnh của bạn có sự hiện diện của TV TCL không?
                    <br />
                    <asp:RadioButton runat="server" ID="YesSelection" GroupName="TCLTivi" Text="có" />
                    <asp:RadioButton runat="server" ID="NoSelection" GroupName="TCLTivi" Text="không" Checked="True" />
                </div>
                <asp:Button runat="server" ID="UploadPhoto" CssClass="button" Text="hoàn tất" Style="margin: 20px auto;" OnClick="UploadPhoto_Click" />
            </asp:View>
            <asp:View runat="server" ID="InformationView">
                <div style="height: 100px;"></div>
                <table>
                    <tr>
                        <td colspan="2">Vui lòng điền đầy đủ các thông tin bên dưới:</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="error">
                                <asp:Literal runat="server" ID="ErrorMessage2"></asp:Literal>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Họ & tên</td>
                        <td>
                            <asp:TextBox runat="server" ID="Fullname" ClientIDMode="Static" />
                        </td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>
                            <asp:TextBox runat="server" ID="Email" ClientIDMode="Static"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Địa chỉ</td>
                        <td>
                            <asp:TextBox runat="server" ID="Address" ClientIDMode="Static"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Số ĐT</td>
                        <td>
                            <asp:TextBox runat="server" ID="PhoneNumber" ClientIDMode="Static"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Số CMND</td>
                        <td>
                            <asp:TextBox runat="server" ID="IDNumber" ClientIDMode="Static"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <asp:Button runat="server" ID="SubmitInformation" OnClick="SubmitInformation_Click" CssClass="button" Text="hoàn tất" />
                        </td>
                    </tr>
                </table>
            </asp:View>
        </asp:MultiView>
    </div>
</asp:Content>
