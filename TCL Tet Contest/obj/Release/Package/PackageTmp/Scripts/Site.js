﻿var W3CDOM = (document.createElement && document.getElementsByTagName);

function initFileUploads() {
    if (!W3CDOM) return;
    var fakeFileUpload = document.createElement('a');
    fakeFileUpload.className = 'button-upload';
    fakeFileUpload.textContent = "Chọn ảnh";
    var x = document.getElementsByTagName('input');
    for (var i = 0; i < x.length; i++) {
        if (x[i].type != 'file') continue;
        x[i].className = 'hidden';
        var clone = fakeFileUpload.cloneNode(true);
        x[i].parentNode.appendChild(clone);
        clone.onclick = function () {
            this.parentNode.getElementsByTagName('input')[1].click();
        };
        x[i].onchange = function () {
            $('form').submit();
        };
    }
}

function showImage(id, path, likes, fullname) {
    $('.image-container img').removeAttr("width");
    $('.image-container img').removeAttr("height");
    $('.image-container img').removeAttr("style");
    $('.image-container img').attr("src", path);
    $('#fullname').text(fullname);
    $('#like-text').text(likes);

    $('#like-message').html("");

    $('.detail-item .like').attr("onclick", "like(" + id + ");");
    $('.detail-item .share').attr("onclick", "shareOnFacebook(" + id + ", '" + path + "');");

    $('.detail-background').fadeIn(function () {
        $('.image-container img').cjObjectScaler({
            method: 'fit',
            fade: 550
        });
    });
}

function closePopup() {
    $('.detail-background').fadeOut();
}

function like(id) {
    $.ajax({
        url: "Like.aspx?id=" + id,
        success: function (result) {
            if (result == "1") {
                $('#like-message').html("<label style='color: #00afff;'>Bạn đã Like bức hình thành công</label>");
                $('.detail-item #like-text').text(parseInt($('.detail-item #like-text').text()) + 1);
            } else {
                $('#like-message').html("<label style='color: #f00;'>Bạn đã Like bức hình này rồi</label>");
            }
        }
    });
}

function shareOnFacebook(id, image) {
    $('#facebookShare').attr("image_id", id);
    $('#facebookShare').attr("image_link", image);
    $('#facebookShare').click();
}
