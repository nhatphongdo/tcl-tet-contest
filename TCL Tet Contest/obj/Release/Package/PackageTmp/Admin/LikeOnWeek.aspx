﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Site1.Master" AutoEventWireup="true" CodeBehind="LikeOnWeek.aspx.cs" Inherits="TCL_Tet_Contest.Admin.LikeOnWeek" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v12.1, Version=12.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:MultiView runat="server" ID="AdminViews">
        <asp:View runat="server" ID="LoginView">
            <asp:Login ID="Login1" runat="server" Font-Names="Arial" Style="margin: auto;" OnAuthenticate="Login1_Authenticate">
            </asp:Login>
        </asp:View>
        <asp:View runat="server" ID="MainView">
            <asp:Panel ID="RadPane1" runat="server" BorderStyle="None" Font-Names="Arial" GroupingText="Thống kê">
                <div style="width: 100%; display: block; text-align:center;">
                    <div style="float:left; margin-right:20px;">
                        <dx:ASPxButton ID="btWeekAt" runat="server" Text="Like cao nhất tại tuần" OnClick="btDayAt_Click"></dx:ASPxButton>                                                      
                    </div>
                    <div style="float:left; padding-top:2px;"><dx:ASPxComboBox ID="DropWeekAt" runat="server" ValueType="System.String"></dx:ASPxComboBox></div>
                </div>
            </asp:Panel>
            <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" Width="100%" KeyFieldName="ID" Theme="Default" OnCustomButtonCallback="ASPxGridView1_CustomButtonCallback">
                <Columns>
                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Visible="False">
                        <ClearFilterButton Visible="True">
                        </ClearFilterButton>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn FieldName="ID" ReadOnly="True" Visible="False" VisibleIndex="1">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="FullName" VisibleIndex="2" Width="40px">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataImageColumn FieldName="PhotoPath" VisibleIndex="7">
                        <PropertiesImage ImageUrlFormatString="~/{0}" ImageWidth="120px">
                        </PropertiesImage>
                    </dx:GridViewDataImageColumn>
                    <dx:GridViewDataCheckColumn FieldName="IsTCL" VisibleIndex="11">
                    </dx:GridViewDataCheckColumn>
                    <dx:GridViewDataDateColumn FieldName="UploadedDate" VisibleIndex="8">
                    </dx:GridViewDataDateColumn>
                    <dx:GridViewDataTextColumn FieldName="NumberLike" VisibleIndex="9">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="IP" VisibleIndex="10">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataCheckColumn FieldName="IsPublished" VisibleIndex="12">
                    </dx:GridViewDataCheckColumn>
                    <dx:GridViewCommandColumn ButtonType="Button" Caption="Published" ShowSelectCheckbox="False" VisibleIndex="13">
                        <CustomButtons>
                            <dx:GridViewCommandColumnCustomButton ID="Publish" Text="Publish/UnPublish">
                                <Image ToolTip="Publish/UnPulish" Url="~/Images/Publish.png"></Image>
                            </dx:GridViewCommandColumnCustomButton>
                        </CustomButtons>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewCommandColumn ButtonType="Button" Caption="IsTCL" ShowSelectCheckbox="False" VisibleIndex="14">
                        <CustomButtons>
                            <dx:GridViewCommandColumnCustomButton ID="IsTCL" Text="TCL/NonTCL">
                                <Image ToolTip="TCL/NonTCL" Url="~/Images/Publish.png"></Image>
                            </dx:GridViewCommandColumnCustomButton>
                        </CustomButtons>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn Caption="PhoneNumber" FieldName="PhoneNumber" VisibleIndex="5">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Address" FieldName="Address" VisibleIndex="4">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Email" FieldName="Email" VisibleIndex="6">
                    </dx:GridViewDataTextColumn>
                </Columns>
                <SettingsPager PageSize="20">
                    <PageSizeItemSettings Visible="True">
                    </PageSizeItemSettings>
                </SettingsPager>
                <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                <Styles>
                    <Cell HorizontalAlign="Center" VerticalAlign="Middle">
                    </Cell>
                </Styles>
            </dx:ASPxGridView>
        </asp:View>
    </asp:MultiView>
</asp:Content>
